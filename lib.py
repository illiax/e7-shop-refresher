import difflib
import logging
import os

import cv2
import numpy as np
import PIL
from desktopmagic.screengrab_win32 import getDisplayRects, getRectAsImage
from pytesseract import Output, image_to_data, pytesseract

from models import *

# pytesseract.tesseract_cmd = "C:\\Program Files\\Tesseract-OCR\\tesseract.exe"  # needed for Windows as OS
user_words_path = os.path.join(os.path.dirname(__file__), "words", "user-words.txt")
CUSTOM_CONFIG = f"--user-words {user_words_path}"

SIMILARITY_RATIO = 0.8
BUY_BUTTONS_AMOUNT = [4, 5]
KSIZE_PARAM = 1

logger = logging.getLogger(__name__)


def find_similar_indexes(target_word, words):
    similar_indexes = []

    for i, word in enumerate(words):
        seq_match = difflib.SequenceMatcher(None, target_word, word)
        similarity_ratio = seq_match.ratio()

        if similarity_ratio > SIMILARITY_RATIO:
            similar_indexes.append(i)

    return similar_indexes


def get_capture(display: int = 0, save: bool = False) -> ImageKit:
    screens = getDisplayRects()
    image = getRectAsImage(screens[display])
    return mkImageKit(image)


def mkImageKit(image: PIL.Image.Image) -> ImageKit:
    img = cv2.cvtColor(np.array(image), cv2.COLOR_BGR2RGB)
    to_gray = lambda img: cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    to_blur = lambda img: cv2.medianBlur(img, KSIZE_PARAM)
    to_threshold = lambda img: cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
    return ImageKit(
        image_PIL=image,
        image_cv2=img,
        grayscale=to_gray(img),
        blur=to_blur(to_gray(img)),
        threshold=to_threshold(to_blur(to_gray(img))),
    )


def find_coords_group(target: str, data: TData) -> list[list[Coord]]:
    return [[mk_coords(index, data) for index in find_similar_indexes(word, data.text)] for word in target.split()]


def get_capture_and_find_target(target: str, display: int = 0, data: TData = None) -> tuple[list[SentenceCoord], TData]:
    if data is None:
        capture = get_capture(display=display, save=True)
        data_dict = image_to_data(capture.grayscale, output_type=Output.DICT, config=CUSTOM_CONFIG)
        data = TData(**data_dict, image_kit=capture)
    coords_group = find_coords_group(target, data)
    if any([len(coords) == 0 for coords in coords_group]):
        logger.debug(f"No target '{target}' partially or completely: {coords_group}.", extra={"mapping": data})
        return [], data
    return select_target_coords(target, coords_group), data


def multi_word_first_strategy(coords: list[list[Coord]]) -> list[SentenceCoord]:
    first_words, *rest = coords
    return [mk_sentence_coord(find_recursive_from_previous_word(coord, rest)) for coord in first_words]


def mk_sentence_coord(coords: list[Coord]) -> SentenceCoord:
    first_coord, *rest = coords
    return SentenceCoord(**first_coord.model_dump(), rest=rest)


def find_recursive_from_previous_word(previous: Coord, coords: list[list[Coord]]) -> list[Coord]:
    if not coords:
        return [previous]
    else:
        next_words, *rest = coords
        next_word = min(next_words, key=lambda coord: abs(coord.top - previous.top))
        return [previous] + find_recursive_from_previous_word(next_word, rest)


def select_target_coords(target: str, coords: list[list[Coord]]) -> list[SentenceCoord]:
    if len(target.split()) > 1:
        return multi_word_first_strategy(coords)
    else:
        return [SentenceCoord(**coord.model_dump()) for coord in coords[0]]


def mk_coords(index: int, data: TData) -> Coord:
    return Coord(
        index=index,
        word=data.text[index],
        top=data.top[index],
        left=data.left[index],
        height=data.height[index],
        width=data.width[index],
    )


def get_closest_buy_coords(
    targets_coords: list[SentenceCoord], buy_coords: list[SentenceCoord]
) -> list[ItemBuyPairCoords]:
    return [
        ItemBuyPairCoords(item=coords, buy=min(buy_coords, key=lambda buy_coord: abs(buy_coord.top - coords.top)))
        for coords in targets_coords
    ]


def get_closest_buy_coords_scrolled(
    targets_coords: list[SentenceCoord], buy_coords: list[SentenceCoord]
) -> list[ItemBuyPairCoords]:
    closest = get_closest_buy_coords(targets_coords, buy_coords)
    scrolled_buy_coords = buy_coords[-2:]
    return [cb for cb in closest if cb.buy.index in [bc.index for bc in scrolled_buy_coords]]


def verify_buy_pairs(buy_coords: list[SentenceCoord]) -> bool:
    if len(buy_coords) in BUY_BUTTONS_AMOUNT:
        pivot_left = buy_coords[0].left
        return all([value.left == pivot_left for value in buy_coords])
    else:
        return False


def print_img(*coords: SentenceCoord, special: list[SentenceCoord] = [], data: TData):
    def _(coords, img, color):
        for coord in coords:
            start = (coord.left, coord.top)
            end = (coord.left + coord.width, coord.top + coord.height)
            if coord.rest:
                last = coord.rest[-1]
                end = (last.left + last.width, last.top + last.height)
            img = cv2.rectangle(img, start, end, color, 2)
        return img

    img = data.image_kit.image_cv2
    img = _(coords, img, (0, 0, 255))
    img = _(special, img, (0, 255, 0))

    cv2.imshow("img", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def find_with_retry(
    target: str, retries: int = 3, condition=lambda x: x, display: int = 0, single=False, data: TData = None
) -> tuple[SentenceCoord, TData]:
    total = retries
    coords, data = get_capture_and_find_target(target, display=display, data=data)
    retries -= 1
    while not condition(coords) and retries >= 1:
        coords, data = get_capture_and_find_target(target, display=display)
        retries -= 1
    logger.debug(f"find with retries {retries}/{total} {target}, {coords}")
    return ((coords[0] if len(coords) != 0 else None) if single else coords), data
