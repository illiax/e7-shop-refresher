from typing import Optional

import numpy
import PIL
from pydantic import BaseModel, ConfigDict, Field


class Coord(BaseModel):
    index: int
    word: str
    top: int
    left: int
    height: int
    width: int


class SentenceCoord(Coord):
    rest: Optional[list[Coord]] = []


class ImageKit(BaseModel):
    model_config = ConfigDict(arbitrary_types_allowed=True)
    image_PIL: PIL.Image.Image
    image_cv2: numpy.ndarray
    grayscale: numpy.ndarray
    blur: numpy.ndarray
    threshold: numpy.ndarray


class TData(BaseModel):
    level: list[int]
    page_num: list[int]
    block_num: list[int]
    par_num: list[int]
    line_num: list[int]
    word_num: list[int]
    left: list[int]
    top: list[int]
    width: list[int]
    height: list[int]
    text: list[str]
    image_kit: ImageKit

    def __str__(self):
        return f"text={self.text}"

    def save_capture(self):
        import os
        from datetime import datetime

        import cv2

        current_datetime = datetime.now().strftime("%Y-%m-%d--%H")
        current_module_directory = os.path.dirname(__file__)
        output_file_path = os.path.join(current_module_directory, f"capture_{current_datetime}.jpg")
        cv2.imwrite(output_file_path, self.image_kit.image_cv2)


class ItemBuyPairCoords(BaseModel):
    item: Coord
    buy: Coord
