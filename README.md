### E7 shop refresher

Thanks to https://gitlab.com/crsuarez/e7-my-shop-refresher. This project is heavily inspired in that project and started as a fork but at the end there were no traces of original code.
https://gitlab.com/crsuarez/e7-my-shop-refresher uses pyautogui to recognize images to look for button interaction but I changed that approach in favour of tesseract and pytesseract lib.


### tesseract
Please follow https://pypi.org/project/pytesseract/ installation guide to install tesseract. 


### how to run
you can check help command. This is a sample and may be outdated:


```
(e7) PS C:\Users\user\code\e7-shop-refresher> python .\refresher.py --help                  
usage: refresher.py [-h] [-f] [-c] [-m] [-d] [-a] [--captures] [--refreshes REFRESHES] [--retries RETRIES]

E7 Shop Refresher

options:
  -h, --help            show this help message and exit
  -f, --friendship      Include "friendship"
  -c, --covenant        Include "covenant"
  -m, --mystic          Include "mystic"
  -d, --debug           Enable debug mode
  -a, --alternative_scroll
                        Uses alternative scroll functionality
  --captures            Enable screen captures
  --refreshes REFRESHES
                        Number of refreshes
  --retries RETRIES     Number of individual retries
```

### Alternative Scroll 

Enable this flag if your game doesn't allow you to use your mouse wheel. This alternative method uses `moveTo` and `dragTo` instead of `scroll` in `pyautogui`