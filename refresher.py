import argparse
import functools
import time

import pyautogui

import lib
from models import Coord, SentenceCoord, TData

logger = None


MAX_LEVEL_LABEL = "Max Level"
REFRESH_LABEL = "3 Refresh"
FRIENDSHIP_BOOKMARK_LABEL = "Friendship Bookmarks"
MYSTIC_MEDAL_LABEL = "Mystic Medal"
COVENANT_BOOKMARK_LABEL = "Covenant Bookmarks"
LEFT_REFRESH_LABEL = "left until refresh"
SECRET_SHOP_LABEL = "Secret Shop Level"
BUY_LABEL = "Buy"
BUY_MESSAGE_LABEL = "Would you like to purchase this product"
CONFIRM_LABEL = "Confirm"


def delay_after_execution(wait_time=1):
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            time.sleep(wait_time)
            return result

        return wrapper

    return decorator


@delay_after_execution()
def left_click(x=int, y=int, clicks=1):
    pyautogui.click(x, y, button="left", clicks=clicks)


LABELS_MAPPING = {
    "friendship": FRIENDSHIP_BOOKMARK_LABEL,
    "mystic": MYSTIC_MEDAL_LABEL,
    "covenant": COVENANT_BOOKMARK_LABEL,
}


def get_targets(args) -> list[str]:
    return [LABELS_MAPPING[arg] for arg in vars(args) if arg in LABELS_MAPPING and getattr(args, arg)]


def scroll_up(alternative_scroll: bool, fromm: Coord, to: Coord):
    if alternative_scroll:
        pyautogui.moveTo(x=fromm.left, y=fromm.top)
        pyautogui.dragTo(x=to.left, y=to.top, duration=2, button="left")
    else:
        pyautogui.scroll(-5000000)
        time.sleep(1)


def main(args):
    total = 0
    targets = get_targets(args)
    logger.debug(f"targets selected {targets}")
    max_level_coord, data = lib.find_with_retry(MAX_LEVEL_LABEL, retries=10, single=True)
    left_click(max_level_coord.left, max_level_coord.top)
    logger.debug(f"clicking in Max Level label to focus window {max_level_coord}")
    refresh_button, data = lib.find_with_retry(REFRESH_LABEL, retries=10, single=True)
    if refresh_button:  # if we are in shop
        logger.debug(f"refresh button found: {refresh_button}")
        while total < args.refreshes:
            logger.debug(f"starting run {total}/{args.refreshes}")
            buy_buttons, data = lib.find_with_retry(BUY_LABEL, retries=20, condition=lambda xs: len(xs) == 4)
            logger.debug(f"buy buttons found: {buy_buttons}")
            shopping_list = []
            for label in targets:
                findings, data = lib.find_with_retry(label, data=data)
                shopping_list = shopping_list + findings
            data = buy_shopping_list(shopping_list, buy_buttons, data)
            time.sleep(1)
            logger.debug("scrolling up")
            scroll_up(args.alternative_scroll, fromm=buy_buttons[-1], to=buy_buttons[-3])
            logger.debug("finished scrolling up")

            buy_buttons, data = lib.find_with_retry(BUY_LABEL, retries=10, condition=lambda xs: len(xs) == 5)
            shopping_list = []
            for label in targets:
                findings, data = lib.find_with_retry(label, data=data)
                shopping_list = shopping_list + findings
            data = buy_shopping_list(shopping_list, buy_buttons, data, scrolled=True)

            data = refresh(refresh_button, data)
            total += 1
            logger.debug(f"ending run {total}/{args.refreshes}")

    else:
        logger.error("NOT IN SHOP")


@delay_after_execution()
def refresh(refresh_button: Coord, data: TData) -> TData:
    logger.debug(f"clicking refresh button {refresh_button}")
    left_click(refresh_button.left, refresh_button.top, clicks=2)
    confirm_button, data = lib.find_with_retry(CONFIRM_LABEL, retries=10, single=True)
    left_click(confirm_button.left, confirm_button.top)
    return data


@delay_after_execution()
def buy_shopping_list(
    shopping_list: list[SentenceCoord], buy_buttons: list[SentenceCoord], data: TData, scrolled: bool = False
):
    logger.debug(f"shopping list to buy: {shopping_list}")
    if shopping_list:
        if not scrolled:
            buy_item_pairs = lib.get_closest_buy_coords(targets_coords=shopping_list, buy_coords=buy_buttons)
        else:
            buy_item_pairs = lib.get_closest_buy_coords_scrolled(targets_coords=shopping_list, buy_coords=buy_buttons)
        logger.info(f"Buy pairs found:{buy_item_pairs}")
        for item in buy_item_pairs:
            left_click(item.buy.left, item.buy.top)
            buy_message_in_popup, data = lib.find_with_retry(BUY_MESSAGE_LABEL, retries=10, data=data)
            if buy_message_in_popup:
                buy_buttons_in_popup, data = lib.find_with_retry(
                    BUY_LABEL, retries=20, condition=lambda xs: len(xs) == 2
                )
                only_buy = buy_buttons_in_popup[1]  # 1 because 0 is windows title : `buy`
                left_click(only_buy.left, only_buy.top)
                logger.info(f"bought {item.item.word}")
    else:
        logger.debug("shopping list empty!!")
    return data


def set_up_logger(debug: bool):
    import logging

    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)
    global logger
    logger = logging.getLogger(__name__)
    return logger


def parse():
    parser = argparse.ArgumentParser(description="E7 Shop Refresher")
    parser.add_argument("-f", "--friendship", help='Include "friendship"', action="store_true")
    parser.add_argument("-c", "--covenant", help='Include "covenant"', action="store_true")
    parser.add_argument("-m", "--mystic", help='Include "mystic"', action="store_true")

    parser.add_argument("-d", "--debug", help="Enable debug mode", action="store_true")
    parser.add_argument("-a", "--alternative_scroll", help="Uses alternative scroll functionality", action="store_true")
    parser.add_argument("--captures", help="Enable screen captures", action="store_true")

    parser.add_argument("--refreshes", type=int, default=1, help="Number of refreshes")
    parser.add_argument("--retries", type=int, default=3, help="Number of individual retries")

    args = parser.parse_args()

    logger = set_up_logger(args.debug)

    if args.friendship or args.covenant or args.mystic:
        main(args)
    else:
        logger.error("shopping list empty")


if __name__ == "__main__":
    parse()
